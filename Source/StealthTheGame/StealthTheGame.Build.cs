// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class StealthTheGame : ModuleRules
{
	public StealthTheGame(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "GameplayTasks", "HeadMountedDisplay" });
	}
}
